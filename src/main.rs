use tokio::net::{TcpListener, TcpStream};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use std::future::Future;
use std::thread::yield_now;
use std::{thread, time};
use std::collections::VecDeque;
use std::{io, str};
use clap::{Parser, Subcommand};
use uuid::Uuid;
use serde::{Serialize, Deserialize};

/// poncho-raft CLI
#[derive(Debug, Parser)] // requires `derive` feature
#[command(name = "poncho-raft")]
#[command(bin_name = "poncho-raft")]
#[command(about = "A CLI for poncho-raft", long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Run a PonchoRaftNode
    Run {
        ip_addr: String,
        port: String,
    },
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Hash)]
struct Message {
    message_type: u8,
    uuid: String, //sender uuid
    message_payload: String, //message payload
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Hash)]
struct Peer {
    uuid: String,
    ip: String,
}

struct PonchoRaftNode {
    tcp_listener: TcpListener,
    message_queue: VecDeque<Message>,
    uuid: Uuid,
    peers: Vec<Peer>,
    raft_state: RaftState,
}

// see https://raft.github.io/raft.pdf
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Hash)]
struct RaftState {
    current_term: i32,
    //voted_for: String,
    //log: Vec<String>,
}


impl PonchoRaftNode {
    async fn run(self) -> Result<(), io::Error> {
        let PonchoRaftNode {
           tcp_listener,
           mut message_queue,
           uuid,
           mut peers,
           mut raft_state,
        } = self;

        println!("Starting PonchoRaftNode on {}:{}", 
            tcp_listener.local_addr().unwrap().ip(), 
            tcp_listener.local_addr().unwrap().port());

        //work done here

        //start election timer in a tokio task
        let _election_timeout_thread = tokio::spawn(async move {
            start_election_timeout().await;
        });

        // start loop for socket handler 
        loop {
            let (mut socket, _) = tcp_listener.accept().await?;
    
            tokio::spawn(async move {
                let mut buf = [0; 1024];
    
                // In a loop, read data from the socket and write the data back.
                loop {
                    let n = match socket.read(&mut buf).await {
                        // socket closed
                        Ok(n) if n == 0 => return,
                        Ok(n) => n,
                        Err(e) => {
                            eprintln!("failed to read from socket; err = {:?}", e);
                            return;
                        }
                    };
    
                    // Write the data back
                    if let Err(e) = socket.write_all(&"ok".as_bytes()).await {
                        eprintln!("failed to write to socket; err = {:?}", e);
                        return;
                    }
                    //print received message
                    println!("Message: {:#?}", str::from_utf8(&buf[0..n]).unwrap());

                    // if message is type RequestVote, cancel the election_timer thread.
                    // TODO: WIP 
                    //
            
                }
            });
        }
    }
}

async fn start_election_timeout() {
    loop {
        println!("Starting Election Timeout");
        let mut election_timer = 10;
        while election_timer > 0 {
            println!("Counting Down Election Timer...{}", election_timer);
            election_timer -= 1;
    
            let sleep_time = time::Duration::from_millis(1000);
            let _ = time::Instant::now();
            thread::sleep(sleep_time);
        }
    
        // election timer countdown reached - increment current term, and transition to candidate state
        println!("Election Timer Reached. Triggering Election.");
        //TODO: WIP
    }

}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {

    let args = Cli::parse();
    match args.command{
        Commands::Run { ip_addr, port } => {
            let bind_addr = format!("{}:{}", ip_addr, port);
            let listener = TcpListener::bind(bind_addr).await?;

            let mut message_queue: VecDeque<Message> = VecDeque::new();
            let uuid = Uuid::new_v4();
            let peers: Vec<Peer>= Vec::new();
            let mut raft_state = RaftState{
                current_term: 0,
            };

            let poncho_raft_node = PonchoRaftNode{
                tcp_listener: listener,
                message_queue: message_queue,
                uuid: uuid,
                peers: peers,
                raft_state: raft_state,
            };
        
            poncho_raft_node.run().await?;
        }
    }

    Ok(())
}
